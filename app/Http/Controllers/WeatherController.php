<?php
// created by artisan: php artisan make:controller weatherController

namespace weather\Http\Controllers;

use Illuminate\Http\Request;
use weather\xml\XMLSerializer;
use weather\weather\WeatherInfoProcessor;

/**
 * Class weatherController
 * @package weather\Http\Controllers
 */
class weatherController extends Controller
{
    /**
     * @param Request $request
     * @return void
     */
    public function getData(Request $request)
    {
        $inputParams = [];
        $inputParams['temperature_scale'] =  $request->input('temperature_scale');
        $inputParams['type_file'] =  $request->input('type_file');
        $this->url = config('variables.url');
        $xmlGenerator = new XMLSerializer;
        $wheaterObject = new WeatherInfoProcessor($this->url, $inputParams, $xmlGenerator);
        $wheaterObject->processData();
    }
}

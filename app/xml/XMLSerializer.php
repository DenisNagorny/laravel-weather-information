<?php

namespace weather\xml;
use \stdClass;

/**
 * Class XMLSerializer
 * @package weather\xml
 */
class XMLSerializer {
    /**
     * @param stdClass $obj
     * @param string $nodeBlock
     * @param string $nodeName
     * @return string
     *
     * functions adopted from  http://www.sean-barton.co.uk/2009/03/turning-an-array-or-object-into-xml-using-php/
     */
    public static function generateValidXmlFromObj(stdClass $obj, $nodeBlock='nodes', $nodeName='node')
    {
         return self::generateValidXmlFromArray(get_object_vars($obj), $nodeBlock,  $nodeName);
    }

    /**
     * @param array $array
     * @param string $nodeBlock
     * @param string $nodeName
     * @return string
     */
    public static function generateValidXmlFromArray($array, $nodeBlock='nodes', $nodeName='node')
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
        $xml .= '<' . $nodeBlock . '>';
        $xml .= self::generateXmlFromArray($array, $nodeName);
        $xml .= '</' . $nodeBlock . '>';

        return $xml;
    }

    /**
     * @param array $array
     * @param string $nodeName
     * @return string
     */
    private static function generateXmlFromArray($array, $nodeName)
    {
         $xml = '';
         if (is_array($array) || is_object($array)) {
             foreach ($array as $key=>$value) {
                 if (is_numeric($key)) {
                     $key = $nodeName;
                 }

                 $xml .= '<' . $key . '>' .
                 self::generateXmlFromArray($value, $nodeName) . '</' . $key . '>';
             }
         } else {
             $xml = htmlspecialchars($array, ENT_QUOTES);
         }

         return $xml;
    }
}

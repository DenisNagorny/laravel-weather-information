<?php
namespace weather\weather;

/**
 * Class WeatherInfoProcessor
 * @package weather\weather
 */
class WeatherInfoProcessor {
    /**
     * @var array
     */
    public $data = NULL;

    /**
     * @var array
     */
    public $inputParams = [];

    /**
     * @var null
     */
    public $xmlGenerator = null;

    /**
     * @var null
     */
    public $processedObject = null;

    /**
     * WeatherInfoProcessor constructor.
     * @param null $url
     * @param array $input_params
     * @param null $xmlGenerator
     */
    function __construct($url = null, $input_params = [], $xmlGenerator = null)
    {
        $this->data = json_decode($this->getWeatherInfo($url));
        $this->inputParams = $input_params;
        $this->xmlGenerator = $xmlGenerator;
    }

    /**
     * @param string $url
     * @return string
     */
    public static function getWeatherInfo($url)
    {
        return file_get_contents($url);
    }

    /**
     * @param $kelvin
     * @return float
     */
    public static function kelvinToCelsius($kelvin)
    {
        return round(($kelvin - 273.15));
    }

    /**
     * @param $kelvin
     * @return float
     */
    public static function kelvinToFahrenheit($kelvin)
    {
        return round((($kelvin - 273.15) * 1.8) + 32);
    }

    /**
     * @param $data
     * @param $type
     */
    private function saveData($data, $type)
    {
        header('Content-disposition: attachment; filename=weather.' . $type);
        header('Content-type: application/' . $type);
        echo $data; 
        exit; 
    }

    /**
     * @return void
     */
    public function processData()
    {
        if (isset($this->inputParams['temperature_scale']) && $this->inputParams['temperature_scale'] === 'celsius') {
            $this->data->main->temp = $this::kelvinToCelsius($this->data->main->temp);
        } else { 
            $this->data->main->temp = $this::kelvinToFahrenheit($this->data->main->temp); 
        } 
        $temp = $this->data->main->temp;
        $this->processedObject = new \stdClass();
        if ($this->inputParams['type_file'] == "json") {
            $this->processedObject->date = $this->data->dt;
            $this->processedObject->wind_direction = $this->data->wind->deg;
            $this->processedObject->temperature = $temp;
            $this->saveData(json_encode($this->processedObject),"json");
        } else if ($this->inputParams['type_file'] == "xml") {
            $this->processedObject->wind_speed = $this->data->wind->speed;
            $this->processedObject->temperature = $temp;
            $this->saveData($this->xmlGenerator->generateValidXmlFromObj($this->processedObject),"xml");
        }
    }
}

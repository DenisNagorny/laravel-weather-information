<form action='{{ URL::to('/getweather') }}' method="POST" >
    {{ csrf_field() }}
    <table>	
        <tr>
            <td><label for="temperature_scale">Temperature scale</label></td>
            <td>
                <select id="temperature_scale" name="temperature_scale">
                    <option value="farenheit">Fahrenheit</option>
                    <option value="celsius">Celsius</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <label for="json_value">JSON</label> 
                <input class="type_file"  id="json_value" name="type_file" checked  value="json" type="radio">
            </td>
            <td></td>
        </tr>
        <tr>
            <td>
                <label for="xml_value">XML </label><input class="type_file" id="xml_value" name="type_file" value="xml" type="radio">
            </td>
            <td></td>
        </tr>
        <tr>
            <td><input type="submit" name="submit" value="Get weather info!" /></td>
        </tr>
    </table>
</form>

@if( ! empty($data['temperature_scale']))
    {{ $data['temperature_scale'] }}
@endif

@if( ! empty($data['type_file']))
    {{ $data['type_file'] }}
@endif

@if( ! empty($data['url']))
    {{ $data['url'] }}
@endif

@if( ! empty($data['static_check']))
    {{ $data['static_check'] }}
@endif

@if( ! empty($data['datajson']))
    <?php print_r($data['datajson']); ?>
@endif



